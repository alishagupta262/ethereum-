var Web3          = require('web3');
var web3          = new Web3('http://206.189.135.167:8545' );

//|| 'http://206.189.135.167:8545'


var address = "0x7379454976378070017fde3e115112439af9c083";
var txHash = "0xd5f511f3e623c76c5b06ea063eea966e054bfa3699cb9fde3d6cd7745b3fd53d";
var signedTransactionData = "0xf861068267608252089486c5befe7fabff4d1d53756b2a8ced283e7f7a340b801ba0e7ce2c04a48d0e102b7f29b9f8b1b9e2cf8be4be6199b92ba09aed5b21e4abd8a011d6b49e36d959b4d031afb9f698bfb2567efe680114026cf2caad9247218f1e"

if (typeof web3 !== 'undefined') {
  web3 = new Web3(web3.currentProvider);
} else {

	console.log("Not websocketing")
  // set the provider you want from Web3.providers
  web3 = new Web3(new 
  Web3.providers.HttpProvider("http://206.189.135.167:8545"));
}




// web3.eth.getTransactionsByAccount({
//   address: "0x7379454976378070017fde3e115112439af9c083"
// } )
// .then(function (result) {
//   console.log("Tx history : " + result) ;
//   // body...
// })





/**
  *
  * Returns the current gas price oracle. The gas price is determined by the last few blocks median gas price.
  *
  */

web3.eth.getGasPrice()
.then(function(result){
  console.log("Gas price :" + result);
});


// web3.eth.estimateGas({
//     from: "0x7379454976378070017fde3e115112439af9c083",
//     to: "0x86c5befe7fabff4d1d53756b2a8ced283e7f7a34",
//     value: "0x3d4ccccd",
//     data: "0xc6888fa10000000000000000000000000000000000000000000000000000000000000003"
// })
// .then(function(result) {
//   console.log("Estimated Gas Price: " + result);
// });

// web3.eth.getAccounts(console.log);

// console.log(web3.eth.getAccounts);

// console.log(web3.eth.defaultBlock)
// web3.eth.getProtocolVersion()
// .then(console.log);

// /**
//   * Returns balance of the address
//   * @params
//   *     {String} - address to get the balance of
//   * @returns 
//   *      {String} A bignumber instance of current balance for the given address
//   */
// 


// account - String|Object: A private key or account object created with web3.eth.accounts.create().

// console.log(web3.eth.accounts.wallet.add(address));


// web3.eth.accounts.wallet(console.log)

web3.eth.getBalance(address)
.then(function(result){
  console.log("Balance of given address is :" + result);
});







// /**
//   * Returns transaction object for a given transaction hash
//   * @params
//   *     {String} - Transaction Hash
//   * @ret
//   *     {object} - A transaction object
//   */

web3.eth.getTransaction(txHash).then( function(result) {
  console.log("Transaction details :" + JSON.stringify(result))
});


// /**
//   * Returns transaction object for a given transaction hash
//   * @params
//   *     {String} - Signed Transaction data in hex format   ; required
//   *     {function} - callback http request                 ; optional
//   * @returns 
//   *     {String} - 32 bytes transaction hash
//   */

 web3.eth.sendSignedTransaction(signedTransactionData).then((result) => console.log("Transaction Hash : " + JSON.stringify(result.transactionHash)));


// 0xd5f511f3e623c76c5b06ea063eea966e054bfa3699cb9fde3d6cd7745b3fd53d
// const serializedTx = 'd5f511f3e623c76c5b06ea063eea966e054bfa3699cb9fde3d6cd7745b3fd53d'


web3.eth.getTransactionCount(address,"pending").then(function(result) {
   console.log("Nonce " + result);
})


// web3.eth.subscribe('pendingTransactions', function(err, res) {
//     console.log('Here')
//     console.log(err)
//     console.log(res)
// }).on('data', function(transaction) {
//     console.log('Here 2')
//     console.log(transaction)
// });




// var subscription = web3.eth.subscribe('logs', {
//                 address: address
//             }).on("data", function(data){
//                 console.log("data >>>>>>>>>>", data);
//             }).on("changed", function(data){
//                 console.log(" changed >>>>>>>>>>", data);
//             });
