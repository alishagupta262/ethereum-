var eth      = require('ethereumjs-tx')

var privateKey = Buffer.from("1197ac6f82db8acf3b018975205993e4550c93b488030fe71b057911c7479892",'hex');

//


  var rawTx = { 

    nonce: 6,         //from the back end 
  
    gasPrice: 26464,   //from the back end    // in Gwei / 1 ether = 1000000000 Gwei = 1000000000000000000 Wei
  
    gasLimit: 21000,  //  web3.toHex(300000)  //in Gwei  / A standard ETH transfer requires a gas limit of 21,000 units of gas.
  
    to: "0x86c5befe7fabff4d1d53756b2a8ced283e7f7a34", 
  
    from: "0x7379454976378070017fde3e115112439af9c083", 
  
    value: 11, //in wei 
  
    data: ''      //empty if to address is not a contract

  }; 

// total gas = gasPrice * gasLimit
// The gas limit is actually the limit because it's the maximum amount of units of gas you are willing to spend on a transaction


const tx = new eth(rawTx);
tx.sign(privateKey);
const serializedTx = tx.serialize()
console.log('0x' + serializedTx.toString('hex'))

//0xf865808253d88252089486c5befe7fabff4d1d53756b2a8ced283e7f7a34843d4ccccd801ca04aab902e6dde31be85afdef015fff19cf8902c36617e017201f023fb2412a02da02a9faf38faa771c99fe49b8b7421228441c6b54875ea1a1e15f722c49746b305



//
// address of account cb01d9ee7a6a395fe28cee539f6056736aa3eaab
// passphrase test123


// console.log(eth.getBaseFee());
